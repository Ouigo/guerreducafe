using Microsoft.VisualStudio.TestTools.UnitTesting;
using Guerre_Du_Café; 
namespace Test_Guerre_Du_Café
{
    [TestClass]
    public class Test_Connecteur
    {
        Connecteur TestConnecteur = new Connecteur(172,16,0,88,1212); 
        [TestMethod]
        public void Test_Reception()
        {
            byte[] TestTab = new byte[1000];
            Assert.AreEqual(TestTab, TestConnecteur.GetTableauDeReception()); 
            TestConnecteur.Recevoir();
            Assert.AreNotEqual(TestTab, TestConnecteur.GetTableauDeReception()); 
        }
    }
}
