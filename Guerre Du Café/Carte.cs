﻿using System;
using System.Collections.Generic;

namespace Guerre_Du_Café
{
    class Carte
    {

        private static Parcelle[,] CarteParcelle;
        List<Ilot> ListeIlot;
        public Carte(string Trame)
        {
            //Constructeur de la carte
            //Création d'un tableau / carte de 100 cases
            CarteParcelle = new Parcelle[10, 10];

            //On divise la trame pour récupérer les lignes de la carte dans un tableau
            String[] TabLigne = Trame.Split('|');
            for (int i = 0; i < TabLigne.Length - 1; i++)
            {
                //On récupère les numéros pour affectuer les bonnes frontières aux bonnes cases
                String[] colonestring = TabLigne[i].Split(':');
                for (int y = 0; y < colonestring.Length; y++)
                {
                    int num2decrypt = Int32.Parse(colonestring[y]);

                    if (num2decrypt > 30)
                    {
                        //Verification si c'est une case mer et affectation de son ID directement pour faciliter la suite
                        if (num2decrypt > 60)
                        {
                            int[] tabfrontiere = NumtoTabcaseautour(num2decrypt - 60);
                            CarteParcelle[i, y] = new Parcelle(i, y, tabfrontiere, 'M');
                        }
                        //Sinon case forêt
                        else
                        {
                            int[] tabfrontiere = NumtoTabcaseautour(num2decrypt - 30);
                            CarteParcelle[i, y] = new Parcelle(i, y, tabfrontiere, 'F');
                        }
                    }
                    //Affectation des id (a, b, c,...) aux ilôt
                    else
                    {
                        int[] tabfrontiere = NumtoTabcaseautour(num2decrypt);
                        CarteParcelle[i, y] = new Parcelle(i, y, tabfrontiere);
                    }
                }
            }
            ApplicationIlot(CarteParcelle);
            affichecarte(CarteParcelle);
        }
        private static void ApplicationIlot(Parcelle[,] carteParcelle)
        {
            //On effectue une affectation par ligne

            //Lettre a affecté aux ilôt
            char derniereLettre = 'a';
            //Sert à éviter overwrite l'id_ilot d'une parcelle en cas d'exception / de cas particulier 
            bool DoesExceptionAppen = true;
            for (int ligne = 0; ligne < 10; ligne++)
            {
                for (int colonne = 0; colonne < 10; colonne++)
                {
                    //Affecte la première case de la carte
                    if (ligne == 0 && colonne == 0 && !(carteParcelle[ligne, colonne].Id_ilot != 'M' && carteParcelle[ligne, colonne].Id_ilot != 'F'))
                    {
                        derniereLettre = '`';
                    }
                    if (ligne == 0 && colonne == 0 && carteParcelle[ligne, colonne].Id_ilot != 'M' && carteParcelle[ligne, colonne].Id_ilot != 'F')
                    {
                        carteParcelle[ligne, colonne].Id_ilot = derniereLettre;
                    }
                    else if (carteParcelle[ligne, colonne].Id_ilot != 'M' && carteParcelle[ligne, colonne].Id_ilot != 'F')
                    {
                        if (carteParcelle[ligne, colonne].Frontieres[0] == 1 && carteParcelle[ligne, colonne].Frontieres[3] == 0 && carteParcelle[ligne, colonne].Frontieres[1] == 1 && carteParcelle[ligne, colonne].Frontieres[2] == 1)
                        {
                            //Nous indique si on est sûr que la case porte bien le l'id qu'on li a accorder
                            bool SUR = false;
                            int indexcolonne = 1;
                            while (!SUR)
                            {
                                if (carteParcelle[ligne, colonne + indexcolonne].Frontieres[0] == 1)
                                {
                                    if (carteParcelle[ligne, colonne + indexcolonne].Frontieres[3] == 1)
                                    {
                                        SUR = true;
                                    }
                                    else
                                    {
                                        indexcolonne += 1;
                                        if (colonne + indexcolonne >= 9)
                                        {
                                            SUR = true;
                                        }
                                    }
                                }
                                else
                                {
                                    carteParcelle[ligne, colonne].Id_ilot = carteParcelle[ligne - 1, colonne + indexcolonne].Id_ilot;
                                    SUR = true;
                                    DoesExceptionAppen = false;
                                }
                            }
                        }
                        if (carteParcelle[ligne, colonne].Frontieres[0] == 0)
                        {
                            carteParcelle[ligne, colonne].Id_ilot = carteParcelle[ligne - 1, colonne].Id_ilot;
                        }
                        else if (carteParcelle[ligne, colonne].Frontieres[1] == 0)
                        {
                            carteParcelle[ligne, colonne].Id_ilot = carteParcelle[ligne, colonne - 1].Id_ilot;
                        }
                        //Si on a gérer un exception
                        else if(DoesExceptionAppen)
                        {
                            derniereLettre++;
                            carteParcelle[ligne, colonne].Id_ilot = derniereLettre;
                        }
                        DoesExceptionAppen = true;
                    }
                }
            }
        }
        private int[] NumtoTabcaseautour(int numtodecrypt)
        {
            int[] tabretour = { 0, 0, 0, 0 };

            //Mise en place des frontière grâce aux multiple de 2
            if (numtodecrypt == 8 || numtodecrypt / 8 == 1)
            {
                numtodecrypt = numtodecrypt % 8;
                tabretour[3] = 1;
            }
            if (numtodecrypt == 4 || numtodecrypt / 4 == 1)
            {
                numtodecrypt = numtodecrypt % 4;
                tabretour[2] = 1;
            }
            if (numtodecrypt == 2 || numtodecrypt / 2 == 1)
            {
                numtodecrypt = numtodecrypt % 2;
                tabretour[1] = 1;
            }
            if (numtodecrypt == 1)
            {
                tabretour[0] = 1;
            }
            return tabretour;
        }
        private static void affichecarte(Parcelle[,] Cartedisplay)
        {
            //Affiche la carte avec les id_ilot à leur emplacement
            for (int i = 0; i < 10; i++)
            {
                for (int y = 0; y < 10; y++)
                {
                    Console.Write(Cartedisplay[i, y].Id_ilot);
                }
                Console.WriteLine("");
            }
        }

        public static char VerifParcelle(int p_ligne, int p_colonne)
        {
            //Modification Tanguy 20/04/2020
            return CarteParcelle[p_ligne, p_colonne].Id_ilot;
        }
    }
}
