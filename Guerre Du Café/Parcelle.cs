﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Guerre_Du_Café
{
    public class Parcelle
    {
        private int y;
        //Prend la lettre de l'ilôt auquel elle appartient ou alors M/F
        char id_ilot;
        //Frontières de l'ilôt
        int[] frontieres;
        private int x;

        public Parcelle()
        {
            this.x = -1;
            this.y = -1;
            this.frontieres = null;
            this.Id_ilot = '1';
        }
        public Parcelle(int x, int y,int[] frontieres)
        {
            this.x = x;
            this.y = y;
            this.frontieres = frontieres;
            this.Id_ilot = '1';
        }
        public Parcelle(int x, int y, int[] frontieres,char chardelacase)
        {
            this.x = x;
            this.y = y;
            this.frontieres = frontieres;
            this.Id_ilot = chardelacase;
        }

        //Getter et setter
        public int X { get => x; set => x = value; }
        public int Y { get => y; set => y = value; }
        public char Id_ilot { get => id_ilot; set => id_ilot = value; }
        public int[] Frontieres { get => frontieres; set => frontieres = value; }
    }
}
