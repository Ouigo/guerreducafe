﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Guerre_Du_Café
{
    class MapGraine
    {
        //Modification Tanguy 20/04/2020
        private static bool[,] m_mapGraine = new bool[10,10];

        public MapGraine()
        {
            //Modification Tanguy 20/04/2020
            for (int ligne = 0; ligne < 10; ligne++)
            {
                for(int colonne = 0; colonne < 10; colonne++)
                {
                    m_mapGraine[ligne, colonne] = false;
                }
            }
        }

        public void AjoutGraine(int p_ligne, int p_colonne)
        {
            //Modification Tanguy 20/04/2020
            m_mapGraine[p_ligne, p_colonne] = true;
        }

        public static bool VerifGraine(int p_ligne, int p_colonne)
        {
            //Modification Tanguy 20/04/2020
            return m_mapGraine[p_ligne, p_colonne];
        }
    }
}
