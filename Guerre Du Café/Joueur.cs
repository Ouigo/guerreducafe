﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Guerre_Du_Café
{
    class Joueur
    {
        private uint v_NombreGraine, v_Score;
        private string v_Nom = "Default";
        private int[] m_coupPrecedent = new int[2];

        public Joueur(string p_Nom)
        {
            this.v_NombreGraine = 28;
            this.v_Score = 0;
            try {
                this.v_Nom = p_Nom;
            } catch(Exception ex) {
                Console.WriteLine(ex.ToString()); 
            }
            m_coupPrecedent[1] = -1;
            m_coupPrecedent[2] = -1;
        }

        public uint V_NombreGraine { get => v_NombreGraine; set => v_NombreGraine = value; }
        public uint V_Score { get => v_Score; set => v_Score = value; }

        public void PoserUneGraine(int CoorX, int CoorY)
        {
            //Modification Tanguy 20/04/2020
            if (CoorX != m_coupPrecedent[1] && CoorY != m_coupPrecedent[2])
            {
                if (LaParcelleEstVierge(CoorX, CoorY))
                {
                    if (LaParcelleEstCultivable(CoorX, CoorY))
                    {
                        if (LaParcelleEstBonne(CoorX, CoorY))
                        {
                            m_coupPrecedent[1] = CoorX; m_coupPrecedent[2] = CoorY;
                        }
                    }
                }
            }
        }

        public bool LaParcelleEstVierge(int p_ligne, int p_colonne)
        {
            //Modification Tanguy 20/04/2020
            if (MapGraine.VerifGraine(p_ligne, p_colonne))
            {
                return false;
            }

            return true;
        }

        public bool LaParcelleEstBonne(int p_ligne, int p_colonne)
        {
            //Modification Tanguy 20/04/2020
            if (m_coupPrecedent[1] == -1 && m_coupPrecedent[2] == -1)
            {
                return true;
            }

            if(p_ligne == m_coupPrecedent[1] || p_colonne == m_coupPrecedent[2])
            {
                return true;
            }

            return false;
        }

        public bool LaParcelleEstCultivable(int p_ligne, int p_colonne)
        {
            //Modification Tanguy 20/04/2020
            if (Carte.VerifParcelle(p_ligne, p_colonne) != 'M' || Carte.VerifParcelle(p_ligne, p_colonne) != 'F')
            {
                return true;
            }

            return false;
        }
    }
}
